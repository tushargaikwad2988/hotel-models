<?php

namespace HotelModels\HotelModels;

use Illuminate\Database\Eloquent\Model;

class HotelTables extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hotel_tables';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'hotel_id',
        'table_id',
        'order_id',
    ];

    /**
     * Get hotel id
     *
     * @return mixed
     */
    public function getHotelId()
    {
        return $this->getAttribute('hotel_id');
    }

    /**
     * Set hotel id
     *
     * @param integer $hotelId
     * @return $this
     */
    public function setHotelId($hotelId)
    {
        $this->setAttribute('hotel_id', $hotelId);

        return $this;
    }

    /**
     * Get table id
     *
     * @return mixed
     */
    public function getTableId()
    {
        return $this->getAttribute('table_id');
    }

    /**
     * Set table id
     *
     * @param integer $tableId
     * @return $this
     */
    public function setTableId($tableId)
    {
        $this->setAttribute('table_id', $tableId);

        return $this;
    }

    /**
     * Get order id
     *
     * @return integer
     */
    public function getOrderId()
    {
        return $this->getAttribute('order_id');
    }

    /**
     * Set order id
     * @param integer $orderId
     * @return $this
     */
    public function setOrderId($orderId)
    {
        $this->setAttribute('order_id', $orderId);

        return $this;
    }
}