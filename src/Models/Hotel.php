<?php

namespace HotelModels\HotelModels;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hotel';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //'organization_id',
        'name',
        'address_line_1',
        'address_line_2',
        'address_line_3',
        'std_code',
        'telephone',
        'mobile',
        'rooms',
        'services',
        'order_date',
        'gst',
        'status',
    ];

    public function rooms()
    {
        return $this->hasMany('App\Rooms');
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->getAttribute('name');
    }

    /**
     * Set name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->setAttribute('name', $name);

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->getAttribute('id');
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->setAttribute('id', $id);

        return $this;
    }

    public function getAddressOne()
    {
        return $this->getAttribute('address_line_1');
    }

    public function setAddressOne($addressLineOne)
    {
        $this->setAttribute('address_line_1', $addressLineOne);

        return $this;
    }

    public function getAddressTwo()
    {
        return $this->getAttribute('address_line_2');
    }

    public function setAddressTwo($addressLineTwo)
    {
        $this->setAttribute('address_line_2', $addressLineTwo);

        return $this;
    }

    public function getAddressThree()
    {
        return $this->getAttribute('address_line_3');
    }

    public function setAddressThree($addressLineThree)
    {
        $this->setAttribute('address_line_3', $addressLineThree);

        return $this;
    }

    public function getMobile()
    {
        return $this->getAttribute('mobile');
    }

    public function setMobile($mobile)
    {
        $this->setAttribute('mobile', $mobile);

        return $this;
    }

    public function getStatus()
    {
        return $this->getAttribute('status');
    }

    public function setStatus($status)
    {
        $this->setAttribute('status', $status);

        return $this;
    }
}