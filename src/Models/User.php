<?php

namespace HotelModels\HotelModels;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    const USER_TYPE_SUPER_ADMIN = 1;
    const USER_TYPE_ADMIN = 2;
    const USER_TYPE_SUB_ADMIN = 3;
    const USER_TYPE_RECEPTIONIST = 4;
    const USER_TYPE_CAPTAIN = 5;
    const USER_TYPE_PANTRY = 6;
    const USER_TYPE_CUSTOMER = 7;

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'user_type_id',
        'last_login',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Check user is valid for organization module
     *
     * @return boolean $isValid
     */
    public function isValidUserForOrganizationList()
    {
        $isValid = false;

        if (in_array($this->attributes['user_type_id'], [
                config('constants.user_type.super_admin'),
                config('constants.user_type.admin')
            ])
        ) {
            $isValid = true;
        }

        return $isValid;
    }

    /**
     * Check user is valid for organization module
     *
     * @return boolean $isValid
     */
    public function isValidUserForAddOrganization()
    {
        $isValid = false;

        if (in_array($this->attributes['user_type_id'], [
            config('constants.user_type.admin')
        ])
        ) {
            $isValid = true;
        }

        return $isValid;
    }

    /**
     * Check user is valid for hotel module
     *
     * @return boolean $isValid
     */
    public function isValidUserForHotelList()
    {
        $isValid = false;

        if (in_array($this->attributes['user_type_id'], [
            config('constants.user_type.super_admin'),
            config('constants.user_type.admin'),
            config('constants.user_type.sub_admin')
        ])
        ) {
            $isValid = true;
        }

        return $isValid;
    }

    /**
     * Check user is valid for hotel module
     *
     * @return boolean $isValid
     */
    public function isValidUserForAddHotel()
    {
        $isValid = false;

        if (in_array($this->attributes['user_type_id'], [
            config('constants.user_type.admin')
        ])
        ) {
            $isValid = true;
        }

        return $isValid;
    }

    /**
     * Check is valid user for employee module
     *
     * @return boolean $isValid
     */
    public function isValidUserForEmployee()
    {
        $isValid = false;

        if (in_array($this->attributes['user_type_id'], [
            config('constants.user_type.super_admin'),
            config('constants.user_type.admin'),
            config('constants.user_type.sub_admin')
        ])
        ) {
            $isValid = true;
        }

        return $isValid;
    }

    /**
     * Check is valid user for food module
     *
     * @return boolean $isValid
     */
    public function isValidUserForFood()
    {
        $isValid = false;

        if (in_array($this->attributes['user_type_id'], [
            config('constants.user_type.super_admin'),
            config('constants.user_type.admin'),
            config('constants.user_type.sub_admin')
        ])
        ) {
            $isValid = true;
        }

        return $isValid;
    }

    /**
     * Check is valid user for order module
     *
     * @return boolean $isValid
     */
    public function isValidUserForOrder()
    {
        $isValid = false;

        if (in_array($this->attributes['user_type_id'], [
            config('constants.user_type.receptionist')
        ])
        ) {
            $isValid = true;
        }

        return $isValid;
    }

    /**
     * Check whether user is admin or not
     *
     * @return boolean $isAdmin
     */
    public function isAdmin()
    {
        $isAdmin = false;

        if (config('constants.user_type.admin') == $this->attributes['user_type_id']) {
            $isAdmin = true;
        }

        return $isAdmin;
    }

    /**
     * Check whether user is sub-admin or not
     *
     * @return boolean $isSubAdmin
     */
    public function isSubAdmin()
    {
        $isSubAdmin = false;

        if (config('constants.user_type.sub_admin') == $this->attributes['user_type_id']) {
            $isSubAdmin = true;
        }

        return $isSubAdmin;
    }

    /**
     * Check is user is catering department
     *
     * @return boolean isCatering
     */
    public function isCatering()
    {
        $catering = false;

        if (config('constants.user_type.catering') == $this->attributes['user_type_id']) {
            $catering = true;
        }

        return $catering;
    }

    /**
     * Check is user is captain
     *
     * @return boolean isCaptain
     */
    public function isCaptain()
    {
        $captain = false;

        if (config('constants.user_type.captain') == $this->attributes['user_type_id']) {
            $captain = true;
        }

        return $captain;
    }

    /**
     * Check is user is pantry
     *
     * @return boolean $pantry
     */
    public function isPantry()
    {
        $pantry = false;

        if (config('constants.user_type.pantry') == $this->attributes['user_type_id']) {
            $pantry = true;
        }

        return $pantry;
    }

    public function customerAddress()
    {
        return $this->hasMany('HotelModels\HotelModels\CustomerAddress', 'user_id', 'id');
    }
}
