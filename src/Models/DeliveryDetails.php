<?php

namespace HotelModels\HotelModels;

use Illuminate\Database\Eloquent\Model;

class DeliveryDetails extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'delivery_details';

    /**
     * Set value for food order id
     *
     * @param $foodOrderId
     * @return FoodCategory $this
     */
    public function setFoodOrderId($foodOrderId)
    {
        $this->attributes['food_order_id'] = $foodOrderId;

        return $this;
    }

    /**
     * Get food order id
     *
     * @return $this
     */
    public function getFoodOrderId()
    {
        return $this->getAttribute('food_order_id');
    }

    /**
     * Get number
     *
     * @return mixed
     */
    public function getNumber()
    {
        return $this->getAttribute('number');
    }

    /**
     * Set number
     *
     * @param integer $number
     * @return FoodCategory $this
     */
    public function setNumber($number)
    {
        $this->setAttribute('number', $number);

        return $this;
    }

    /**
     * Get address
     * @return mixed
     */
    public function getAddress()
    {
        return $this->getAttribute('address');
    }

    /**
     * Set address
     *
     * @param string $address
     * @return FoodCategory $this
     */
    public function setAddress($address)
    {
        $this->setAttribute('address', $address);
    }



    public function getCustomerId()
    {
        return $this->getAttribute('customer_id');
    }

    public function setCustomerId($customerId)
    {
        $this->setAttribute('customer_id', $customerId);

        return $this;
    }

    public function getAddressId()
    {
        return $this->getAttribute('address_id');
    }

    public function setAddressId($addressId)
    {
        $this->setAttribute('addressId', $addressId);

        return $this;
    }
}