<?php

namespace HotelModels\HotelModels;

use Illuminate\Database\Eloquent\Model;

class UserHotelRelation extends Model
{
    protected $table = 'user_hotel_relation';

    protected $fillable = [
        'hotel_id', 'user_id',
    ];

    /**
     * Get hotel id
     *
     * @return integer
     */
    public function getHotelId()
    {
        return $this->getAttribute('hotel_id');
    }

    /**
     * Set hotel id
     *
     * @return $this
     */
    public function setHotelId($hotelId)
    {
        $this->setAttribute('hotel_id', $hotelId);

        return $this;
    }

    /**
     * Get user id
     *
     * @return mixed
     */
    public function getUserId()
    {
        return $this->getAttribute('user_id');
    }

    /**
     * Set user id
     *
     * @param integer $userId
     * @return $this
     */
    public function setUserId($userId)
    {
        $this->setAttribute('user_id', $userId);

        return $this;
    }
}
