<?php

namespace HotelModels\HotelModels;

use Illuminate\Database\Eloquent\Model;

class RoomServices extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'room_services';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * Get name of services
     *
     * @return string
     */
    public function getName()
    {
        return $this->getAttribute('name');
    }

    /**
     * Set name of services
     *
     * @param string $name
     * @return $this RoomServices
     */
    public function setName($name)
    {
        $this->setAttribute('name', $name);

        return $this;
    }
}
