<?php

namespace HotelModels\HotelModels;

use Illuminate\Database\Eloquent\Model;

class FoodOrderItemLog extends Model
{
    const STATUS_CANCELED = 0;
    const STATUS_SAVE = 1;
    const STATUS_CONFIRMED = 2;
    const STATUS_READY_TO_DELIVER = 3;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'food_order_item_log';
    public $timestamps = false;

    protected $fillable = [
        'food_order_id',
        'item_id',
        'qty',
        'status',
        'archived',
    ];

    /**
     * Get food order id
     *
     * @return mixed
     */
    public function getFoodOrderId()
    {
        return $this->getAttribute('food_order_id');
    }

    /**
     * Set food order id
     *
     * @param integer $foodOrderId
     * @return $this
     */
    public function setFoodOrderId($foodOrderId)
    {
        $this->setAttribute('food_order_id', $foodOrderId);

        return $this;
    }

    /**
     * Get item id
     *
     * @return mixed
     */
    public function getItemId()
    {
        return $this->getAttribute('item_id');
    }

    /**
     * Set item id
     *
     * @param integer $itemId
     * @return $this
     */
    public function setItemId($itemId)
    {
        $this->setAttribute('item_id', $itemId);

        return $this;
    }

    /**
     * Get quantity
     *
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->getAttribute('qty');
    }

    /**
     * Set quantity
     *
     * @param integer $qty
     * @return $this
     */
    public function setQuantity($qty)
    {
        $this->setAttribute('qty', $qty);

        return $this;
    }


    /**
     * Get archived field
     *
     * @return mixed
     */
    public function getArchived()
    {
        return $this->getAttribute('archived');
    }

    /**
     * Set archived
     *
     * @param $archived
     * @return $this
     */
    public function setArchived($archived)
    {
        $this->setAttribute('archived', $archived);

        return $this;
    }

    /**
     * Get status
     *
     * @return mixed
     */
    public function getStatus()
    {
        return $this->getAttribute('status');
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->setAttribute('status', $status);

        return $this;
    }
}
