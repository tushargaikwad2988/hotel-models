<?php

namespace HotelModels\HotelModels;

use Illuminate\Database\Eloquent\Model;

class FoodType extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'food_type';
}
