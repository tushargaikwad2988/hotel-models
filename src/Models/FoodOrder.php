<?php

namespace HotelModels\HotelModels;

use Illuminate\Database\Eloquent\Model;

class FoodOrder extends Model
{
    /** Constant for order status */
    const ORDER_STATUS_CANCELLED = 0;
    const ORDER_STATUS_SAVE = 1;
    const ORDER_STATUS_CONFIRMED = 2;
    const ORDER_STATUS_READY_TO_DELIVER = 3;
    const ORDER_STATUS_PAID = 5;

    /** Constant for receipt taken status */
    const RECEIPT_TAKEN_YES = 1;
    const RECEIPT_TAKEN_NO = 0;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'food_order';

    protected $fillable = [
        'bill_number',
        'status',
        'hotel_id',
        'user_id',
        'table_id',
        'sgst',
        'sgst_amount',
        'cgst',
        'cgst_amount',
        'discount',
        'sub_total',
        'grand_total',
        'coupon',
        'payment_mode',
        'receipt_taken',
        'status',
        'order_date',
        'comment',
    ];

    /**
     * Get receipt taken
     *
     * @return boolean
     */
    public function getReceiptTaken()
    {
        return $this->getAttribute('receipt_taken');
    }

    /**
     * Set receipt taken
     *
     * @param boolean $receiptTaken
     * @return $this
     */
    public function setReceiptTaken($receiptTaken)
    {
        $this->setAttribute('receipt_taken', $receiptTaken);

        return $this;
    }

    /**
     * Get bill number
     *
     * @return mixed
     */
    public function getBillNumber()
    {
        return $this->getAttribute('bill_number');
    }

    /**
     * Set bill number
     *
     * @param integer $billNumber
     * @return $this
     */
    public function setBillNumber($billNumber)
    {
        $this->setAttribute('bill_number', $billNumber);

        return $this;
    }

    /**
     * Get order status
     *
     * @param integet $lable
     * @return string
     */
    public function getStatus($lable = false)
    {
        $orderStatusList = config('constants.order_status_label');

        if (true == $lable && array_key_exists($this->getAttribute('status'), $orderStatusList)) {
            return $orderStatusList[$this->getAttribute('status')];
        }

        return $this->getAttribute('status');
    }

    /**
     * Set order status
     *
     * @param integer $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->setAttribute('status', $status);

        return $this;
    }

    /**
     * Get hotel id
     *
     * @return integer
     */
    public function getHotelId()
    {
        return $this->getAttribute('hotel_id');
    }

    /**
     * Set hotel id
     *
     * @param integer $hotelId
     * @return $this
     */
    public function setHotelId($hotelId)
    {
        $this->setAttribute('hotel_id', $hotelId);

        return $this;
    }

    /**
     * Get table id
     *
     * @return integer
     */
    public function getTableId()
    {
        return $this->getAttribute('table_id');
    }

    /**
     * Set table id
     *
     * @param integer $tableId
     * @return $this
     */
    public function setTableId($tableId)
    {
        $this->setAttribute('table_id', $tableId);

        return $this;
    }

    /**
     * Get sgst amount
     *
     * @return double
     */
    public function getSgstAmount()
    {
        return $this->getAttribute('sgst_amount');
    }

    /**
     * Set sgst amount
     *
     * @param double $sgst
     * @return $this
     */
    public function setSgstAmount($sgst)
    {
        $this->setAttribute('sgst_amount', $sgst);

        return $this;
    }

    /**
     * Get cgst amount
     *
     * @return double
     */
    public function getCgstAmount()
    {
        return $this->getAttribute('cgst_amount');
    }

    /**
     * Set cgst amount
     *
     * @param double $cgst
     * @return $this
     */
    public function setCgstAmount($cgst)
    {
        $this->setAttribute('cgst_amount', $cgst);

        return $this;
    }

    /**
     * Get total of order
     *
     * @return double
     */
    public function getTotal()
    {
        return round($this->getAttribute('grand_total'), 1);
    }

    /**
     * Set total of order
     *
     * @param double $total
     * @return $this
     */
    public function setTotal($total)
    {
        $this->setAttribute('grand_total', round($total, 1));

        return $this;
    }

    public function getRemark()
    {
        return $this->getAttribute('remark');
    }

    public function setRemark($remark)
    {
        $this->setAttribute('remark', $remark);

        return $this;
    }

    public function getDiscount()
    {
        return $this->getAttribute('discount');
    }

    public function setDiscount($discount)
    {
        $this->setAttribute('discount', $discount);

        return $this;
    }

    public function getComment()
    {
        return $this->getAttribute('comment');
    }

    public function setComment($comment)
    {
        $this->setAttribute('comment', $comment);

        return $this;
    }

    public function setUserId($userId)
    {
        $this->setAttribute('user_id', $userId);

        return $this;
    }

    public function getUserId()
    {
        return $this->getAttribute('user_id');
    }

    public function setCustomerId($customerId)
    {
        $this->setAttribute('customer_id', $customerId);

        return $this;
    }

    public function getCustomerId()
    {
        return $this->getAttribute('customer_id');
    }

    public function setRemoteAddress($remoteAddr)
    {
        $this->setAttribute('remote_addr', $remoteAddr);

        return $this;
    }

    public function getRemoteAddress()
    {
        return $this->getAttribute('remote_addr');
    }

    public function getSgst()
    {
        return $this->getAttribute('sgst');
    }

    public function setSgst($sgst)
    {
        $this->setAttribute('sgst', $sgst);

        return $this;
    }

    public function getCgst()
    {
        return $this->getAttribute('cgst');
    }

    public function setCgst($cgst)
    {
        $this->setAttribute('cgst', $cgst);

        return $this;
    }

    public function getPaymentMode()
    {
        return $this->getAttribute('payment_mode');
    }

    public function setPaymentMode($paymentMode)
    {
        $this->setAttribute('payment_mode', $paymentMode);

        return $this;
    }

}