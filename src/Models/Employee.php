<?php

namespace HotelModels\HotelModels;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'employee';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'mobile',
        'alt_mobile',
        'birth_date',
        'type_id',
        'address',
        'identity_proof_id',
        'address_proof_id',
        'profile_image',
        'document_image',
        'status',
    ];

    public function getUserId()
    {
        return $this->getAttribute('user_id');
    }

    public function setUserId($userId)
    {
        $this->setAttribute('user_id', $userId);

        return $this;
    }

    public function getMobile()
    {
        return $this->getAttribute('mobile');
    }

    public function setMobile($mobile)
    {
        $this->setAttribute('mobile', $mobile);

        return $this;
    }

    public function getBirthDate()
    {
        return $this->getAttribute('birth_date');
    }

    public function setBirthDate($birthDate)
    {
        $this->setAttribute('birth_date', $birthDate);

        return $this;
    }

    public function getTypeId()
    {
        return $this->getAttribute('type_id');
    }

    public function setTypeId($typeId)
    {
        $this->setAttribute('type_id', $typeId);

        return $this;
    }

    public function getAddress()
    {
        return $this->getAttribute('address');
    }

    public function setAddress($address)
    {
        $this->setAttribute('address', $address);

        return $this;
    }
}