<?php

namespace HotelModels\HotelModels;

use Illuminate\Database\Eloquent\Model;

class TableSection extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'table_section';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'hotel_id',
        'name',
        'abbreviation',
        'noOfTables'
    ];

    /**
     * Get food details
     */
    public function hotel()
    {
        return $this->belongsTo('App\Hotel');
    }

    /**
     * Get hotel id
     *
     * @return mixed
     */
    public function getHotelId()
    {
        return $this->getAttribute('hotel_id');
    }

    /**
     * Set hotel id
     *
     * @param integer $hotelId
     * @return $this
     */
    public function setHotelId($hotelId)
    {
        $this->setAttribute('hotel_id', $hotelId);

        return $this;
    }

    /**
     * Get name
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->getAttribute('name');
    }

    /**
     * Set name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->setAttribute('name', $name);

        return $this;
    }

    /**
     * Get abbreviation
     *
     * @return mixed
     */
    public function getAbbreviation()
    {
        return $this->getAttribute('abbreviation');
    }

    /**
     * Set abbreviation
     *
     * @param string $abbreviation
     * @return $this
     */
    public function setAbbreviation($abbreviation)
    {
        $this->setAttribute('abbreviation', $abbreviation);

        return $this;
    }

    /**
     * Get number of tables
     *
     * @return mixed
     */
    public function getNumberOfTables()
    {
        return $this->getAttribute('no_of_tables');
    }

    /**
     * Set number of tables
     *
     * @param integer $noOfTables
     * @return $this
     */
    public function setNumberOfTables($noOfTables)
    {
        $this->setAttribute('no_of_tables', $noOfTables);

        return $this;
    }
}
