<?php

namespace HotelModels\HotelModels;

use Illuminate\Database\Eloquent\Model;

class RoomCategory extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'room_category';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * Get attribute name
     *
     * @return string
     */
    public function getName()
    {
        return $this->getAttribute('name');
    }

    /**
     * Set attribute name value
     *
     * @param string $name
     * @return RoomCategory $this
     */
    public function setName($name)
    {
        $this->setAttribute('name', $name);

        return $this;
    }
}
