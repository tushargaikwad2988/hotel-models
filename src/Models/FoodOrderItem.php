<?php

namespace HotelModels\HotelModels;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class FoodOrderItem extends Model
{
    const STATUS_CANCELED = 0;
    const STATUS_SAVE = 1;
    const STATUS_CONFIRMED = 2;
    const STATUS_READY_TO_DELIVER = 3;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'food_order_item';

    protected $fillable = [
        'food_order_id',
        'item_id',
        'qty',
        'discount',
        //'status',
        'archived',
        'created_at',
        'updated_at',
        'comment',
        'property',
    ];

    protected $primaryKey = 'id';

    /**
     * Belongs name form food table
     */
    public function food()
    {
        $this->belongsTo('App\Food', 'item_id', 'id');
    }

    /**
     * Get id
     */
    public function getId()
    {
        return $this->getAttribute('id');
    }

    /**
     * Get update at date diff in min
     *
     * @return integer
     */
    public function getUpdatedAtDiffInMinutes()
    {
        $currentTime = Carbon::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s'));

        $updatedAtTime = Carbon::createFromFormat('Y-m-d H:i:s', $this->getAttribute('updated_at')->toDateTimeString());

        return $updatedAtTime->diffInMinutes($currentTime);
    }

    /**
     * Get food order id
     *
     * @return mixed
     */
    public function getFoodOrderId()
    {
        return $this->getAttribute('food_order_id');
    }

    /**
     * Set food order id
     *
     * @param integer $foodOrderId
     * @return $this
     */
    public function setFoodOrderId($foodOrderId)
    {
        $this->setAttribute('food_order_id', $foodOrderId);

        return $this;
    }

    /**
     * Get item id
     *
     * @return mixed
     */
    public function getItemId()
    {
        return $this->getAttribute('item_id');
    }

    /**
     * Set item id
     *
     * @param integer $itemId
     * @return $this
     */
    public function setItemId($itemId)
    {
        $this->setAttribute('item_id', $itemId);

        return $this;
    }

    /**
     * Get quantity
     *
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->getAttribute('qty');
    }

    /**
     * Set quantity
     *
     * @param integer $qty
     * @return $this
     */
    public function setQuantity($qty)
    {
        $this->setAttribute('qty', $qty);

        return $this;
    }

    /**
     * Get status
     *
     * @return mixed
     */
    /*public function getStatus()
    {
        return $this->getAttribute('status');
    }*/

    /**
     * Set status
     *
     * @param integer $status
     * @return $this
     */
    /*public function setStatus($status)
    {
        $this->setAttribute('status', $status);

        return $this;
    }*/

    /**
     * Get archived field
     *
     * @return mixed
     */
    public function getArchived()
    {
        return $this->getAttribute('archived');
    }

    /**
     * Set archived
     *
     * @param $archived
     * @return $this
     */
    public function setArchived($archived)
    {
        $this->setAttribute('archived', $archived);

        return $this;
    }

    public function getComment()
    {
        return $this->getAttribute('comment');
    }

    public function setComment($comment)
    {
        $this->setAttribute('comment', $comment);

        return $this;
    }

    public function getProperty()
    {
        return $this->getAttribute('property');
    }

    public function setProperty($property)
    {
        $this->setAttribute('property', $property);

        return $this;
    }

    public function getUnitPrice()
    {
        return $this->getAttribute('unit_price');
    }

    public function setUnitPrice($unitPrice)
    {
        $this->setAttribute('unit_price', $unitPrice);

        return $this;
    }
}
