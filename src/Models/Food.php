<?php

namespace HotelModels\HotelModels;

use Illuminate\Database\Eloquent\Model;

class Food extends Model
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'foods';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'hotel_id',
        'category_id',
        'name',
        'abbreviation',
        'amount',
        'description',
        'discount',
        'status',
        'special',
        'is_pantry',
    ];

    /**
     * Food category join
     */
    public function foodCategory()
    {
        $this->belongsTo('App\FoodCategory');
    }

    public function foodOrderItem()
    {
        $this->hasMany('App\FoodOrderItem');
    }

    public function specialMenu()
    {
        $this->hasMany('App\SpecialMenu');
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->getAttribute('name');
    }

    /**
     * Set name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->setAttribute('name', $name);

        return $this;
    }

    /**
     * Get hotel id
     *
     * @return integer
     */
    public function getHotelId()
    {
        return $this->getAttribute('hotel_id');
    }

    /**
     * Set hotel id
     *
     * @param integer $hotelId
     * @return $this
     */
    public function setHotelId($hotelId)
    {
        $this->setAttribute('hotel_id', $hotelId);

        return $this;
    }

    /**
     * Get amount
     *
     * @return double
     */
    public function getAmount()
    {
        return $this->getAttribute('amount');
    }

    /**
     * Set amount
     *
     * @param double $amount
     * @return $this
     */
    public function setAmount($amount)
    {
        $this->setAttribute('amount', $amount);

        return $this;
    }

    /**
     * Get description
     * @return mixed
     */
    public function getDescription()
    {
        return $this->getAttribute('description');
    }

    /**
     * Set description
     *
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->setAttribute('description', $description);

        return $this;
    }

    /**
     * Get discount
     *
     * @return mixed
     */
    public function getDiscount()
    {
        return $this->getAttribute('discount');
    }

    /**
     * Set discount
     *
     * @param integer $discount
     * @return $this
     */
    public function setDiscount($discount)
    {
        $this->setAttribute('discount', $discount);

        return $this;
    }

    /**
     * Get food is special
     *
     * @return integer
     */
    public function getIsSpecial()
    {
        return $this->getAttribute('special');
    }

    /**
     * Set is food special
     *
     * @param integer $special
     * @return $this
     */
    public function setIsSpecial($special)
    {
        $this->setAttribute('special', $special);

        return $this;
    }

    /**
     * Get abbreviation
     *
     * @return string
     */
    public function getAbbreviation()
    {
        return $this->getAttribute('abbreviation');
    }

    /**
     * Set abbreviation
     *
     * @param string $abbreviation
     * @return $this
     */
    public function setAbbreviation($abbreviation)
    {
        $this->setAttribute('abbreviation', $abbreviation);

        return $this;
    }

    /**
     * Get category id
     *
     * @return integer
     */
    public function getCategoryId()
    {
        return $this->getAttribute('category_id');
    }

    /**
     * Set category id
     *
     * @param integer $categoryId
     * @return $this
     */
    public function setCategoryId($categoryId)
    {
        $this->setAttribute('category_id', $categoryId);

        return $this;
    }

    /**
     * Get id
     * @return integer
     */
    public function getId()
    {
        return $this->getAttribute('id');
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->setAttribute('id', $id);

        return $this;
    }

    /**
     * Set food id
     *
     * @param integer $id
     * @return $this
     */
    public function setFoodId($id)
    {
        $this->setAttribute('food_id', $id);

        return $this;
    }

    public function getIsPantry()
    {
        return $this->getAttribute('is_pantry');
    }

    public function setIsPantry($isPantry)
    {
        $this->setAttribute('is_pantry', $isPantry);

        return $this;
    }
}
