<?php

namespace HotelModels\HotelModels;

use Illuminate\Database\Eloquent\Model;

class Rooms extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'rooms';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'room_id',
        'hotel_id',
        'category_id',
        'services',
        'rent',
        'discount',
        'description',
    ];

    /**
     * Get room id
     *
     * @return integer
     */
    public function getRoomId()
    {
        return $this->getAttribute('room_id');
    }

    /**
     * Set room id
     *
     * @param integer $roomId
     * @return Rooms $this
     */
    public function setRoomId($roomId)
    {
        $this->setAttribute('room_id', $roomId);

        return $this;
    }

    /**
     * Get hotel id
     *
     * @return integer
     */
    public function getHotelId()
    {
        return $this->getAttribute('hotel_id');
    }

    /**
     * Set hotel id
     *
     * @param integer $hotelId
     * @return Rooms $this
     */
    public function setHotelId($hotelId)
    {
        $this->setAttribute('hotel_id', $hotelId);

        return $this;
    }

    /**
     * Get category id
     *
     * @return integer
     */
    public function getCategoryId()
    {
        return $this->getAttribute('category_id');
    }

    /**
     * Set category id
     *
     * @param integer $categoryId
     * @return Rooms $this
     */
    public function setCategoryId($categoryId)
    {
        $this->setAttribute('category_id', $categoryId);

        return $this;
    }

    /**
     * Get services
     *
     * @return array
     */
    public function getServices()
    {
        return explode(',', $this->getAttribute('services'));
    }

    /**
     * Set services
     *
     * @param array $services
     * @return Rooms $this
     */
    public function setServices($services)
    {
        $this->setAttribute('services', implode(',', $services));

        return $this;
    }

    /**
     * Get rent
     *
     * @return double
     */
    public function getRent()
    {
        return $this->getAttribute('rent');
    }

    /**
     * Set rent
     *
     * @param double $rent
     * @return Rooms $this
     */
    public function setRent($rent)
    {
        $this->setAttribute('rent', $rent);

        return $this;
    }

    /**
     * Get discount
     *
     * @param integer $discount
     * @return double
     */
    public function getDiscount()
    {
        return $this->getAttribute('discount');
    }

    /**
     * Set discount
     *
     * @param double $discount
     * @return Rooms $this
     */
    public function setDiscount($discount)
    {
        $this->setAttribute('discount', $discount);

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->getAttribute('description');
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Rooms $this
     */
    public function setDescription($description)
    {
        $this->setAttribute('description', $description);

        return $this;
    }

    /**
     * Get room services list
     *
     * @param string $serviceIds
     * @param array $services
     * @return array $list
     */
    public function getRoomService($serviceIds, $services)
    {
        $serviceIds = explode(',', $serviceIds);

        $list = [];
        foreach ($serviceIds as $service) {
            $list[$service] = $services[$service]->name;
        }

        return implode(', ', $list);
    }

    /**
     * Belongs to hotel table
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function hotel()
    {
        return $this->belongsTo('App\Hotel');
    }

    /**
     * Belongs to room category table
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\RoomCategory');
    }
}
