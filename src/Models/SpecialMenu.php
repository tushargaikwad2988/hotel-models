<?php

namespace HotelModels\HotelModels;

use Illuminate\Database\Eloquent\Model;

class SpecialMenu extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'special_menu';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'hotel_id',
        'food_id',
        'amount',
        'description',
        'discount',
    ];

    /**
     * Get hotel id
     *
     * @return integer
     */
    public function getHotelId()
    {
        return $this->getAttribute('hotel_id');
    }

    /**
     * Set hotel id
     *
     * @param integer $hotelId
     * @return $this
     */
    public function setHotelId($hotelId)
    {
        $this->setAttribute('hotel_id', $hotelId);

        return $this;
    }

    /**
     * Get food id
     *
     * @return integer
     */
    public function getFoodId()
    {
        return $this->getAttribute('food_id');
    }

    /**
     * Set food id
     *
     * @param integer $foodId
     * @return $this
     */
    public function setFoodId($foodId)
    {
        $this->setAttribute('food_id', $foodId);

        return $this;
    }

    /**
     * Get amount
     *
     * @return double
     */
    public function getAmount()
    {
        return $this->getAttribute('amount');
    }

    /**
     * Set amount
     *
     * @param double $amount
     * @return $this
     */
    public function setAmount($amount)
    {
        $this->setAttribute('amount', $amount);

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->getAttribute('description');
    }

    /**
     * Set description
     *
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->setAttribute('description', $description);

        return $this;
    }

    /**
     * Get discount
     *
     * @return integer
     */
    public function getDiscount()
    {
        return $this->getAttribute('discount');
    }

    /**
     * Set discount
     *
     * @param integer $discount
     * @return $this
     */
    public function setDiscount($discount)
    {
        $this->setAttribute('discount', $discount);

        return $this;
    }

    /**
     * Get food details
     */
    public function food()
    {
        return $this->belongsTo('App\Food');
    }
}
