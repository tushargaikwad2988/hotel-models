<?php

namespace HotelModels\HotelModels;

use Illuminate\Database\Eloquent\Model;

class EmployeeType extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'employee_type';
}
