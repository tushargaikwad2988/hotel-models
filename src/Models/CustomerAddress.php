<?php

namespace HotelModels\HotelModels;

use Illuminate\Database\Eloquent\Model;

class CustomerAddress extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'customer_address';

    protected $fillable = [
        'user_id',
        'address_type',
        'name',
        'mobile',
        'alt_mobile',
        'address',
        'landmark',
    ];

    protected $attributes = [
        'pincode' => '411028',
        'locality' => 'Ranjangaon',
        'city' => 'Pune',
    ];

    public function getAddressType()
    {
        return $this->getAttribute('address_type');
    }

    public function setAddressType($addressType)
    {
        $this->setAttribute('address_type', $addressType);

        return $this;
    }

    public function getName()
    {
        return $this->getAttribute('name');
    }

    public function setName($name)
    {
        $this->setAttribute('name', $name);

        return $this;
    }

    public function getMobile()
    {
        return $this->getAttribute('mobile');
    }

    public function setMobile($mobile)
    {
        $this->setAttribute('mobile', $mobile);

        return $this;
    }

    public function setAttribute($mobile)
    {
        $this->setAttribute('mobile', $mobile);

        return $this;
    }

    public function getAlternateMobile()
    {
        return $this->getAttribute('alt_mobile');
    }

    public function setAlternateMobile($altMobile)
    {
        $this->setAttribute('alt_mobile', $altMobile);

        return $this;
    }

    public function getAddress()
    {
        return $this->getAttribute('address');
    }

    public function setAddress($address)
    {
        $this->setAttribute('address', $address);

        return $this;
    }

    public function getLandmark()
    {
        return $this->getAttribute('landmark');
    }

    public function setLandmark($landmark)
    {
        $this->setAttribute('landmark', $landmark);

        return $this;
    }

    public function getUserId()
    {
        return $this->getAttribute('user_id');
    }

    public function setUserId($userId)
    {
        $this->setAttribute('user_id', $userId);

        return $this;
    }
}