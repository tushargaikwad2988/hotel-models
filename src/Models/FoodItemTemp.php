<?php

namespace HotelModels\HotelModels;

use Illuminate\Database\Eloquent\Model;

class FoodItemTemp extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'food_item_temp';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'hotel_id',
        'category_id',
        'name',
        'abbreviation',
        'description',
        'amount',
        'discount',
        'is_pantry',
    ];

    /**
     * Get the post that owns the comment.
     */
    public function foodCategory()
    {
        return $this->belongsTo('App\FoodCategory', 'category_id', 'id');
    }
}
