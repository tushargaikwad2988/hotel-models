<?php

namespace HotelModels\HotelModels;

use Illuminate\Database\Eloquent\Model;

class BillNumber extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'bill_number';

    protected $fillable = [
        'hotel_id',
        'bill_number',
    ];

    public function getHotelId()
    {
        return $this->getAttribute('hotel_id');
    }

    public function setHotelId($hotelId)
    {
        $this->setAttribute('hotel_id', $hotelId);

        return $this;
    }

    public function getBillNumber()
    {
        return $this->getAttribute('bill_number');
    }

    public function setBillNumber($billNumber)
    {
        $this->setAttribute('bill_number', $billNumber);

        return $this;
    }
}
