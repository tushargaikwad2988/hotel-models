<?php
namespace HotelModels\HotelModels;

use HotelModels\HotelModels\FoodOrderItemLog;

class FoodOrderItemLogRepository
{
    /**
     * Save food order item log for respective order
     *
     * @param array $data
     * @param integer $orderId
     * @param integer $status
     * @return bool|mixed
     */
    public function save($data, $orderId, $status = FoodOrderItemLog::STATUS_CONFIRMED)
    {
        try {
            $foodOrderItemLog = new FoodOrderItemLog();
            $foodOrderItemLog->food_order_id = $orderId;
            $foodOrderItemLog->item_id = $data['id'];
            $foodOrderItemLog->qty = $data['qty'];
            $foodOrderItemLog->status = $status;

            $foodOrderItemLog->save();
        } catch (Exception $e) {
            return false;
        }

        return $foodOrderItemLog->id;
    }

    /**
     * Find by condition
     *
     * @param array $where
     * @return mixed
     */
    public function findByCondition($where)
    {
        $foodLog = FoodOrderItemLog::where($where)->first();

        return $foodLog;
    }

    /**
     * Update field by condition
     *
     * @param array $condition
     * @param array $field
     * @return boolean $status
     */
    public function updateByCondition($condition, $field)
    {
        $status = FoodOrderItemLog::where($condition)->update($field);

        return $status;
    }

    /**
     * Get orders for catering department
     *
     * @param integer $hotelId
     * @return \Illuminate\Support\Collection
     */
    public function getCateringOrders($hotelId)
    {
        $orders = FoodOrder::where([
            'food_order.hotel_id' => $hotelId,
            'food_order.status' => FoodOrder::ORDER_STATUS_CONFIRMED,
        ])
            ->leftJoin('food_order_item_log as foil', [
                ['foil.food_order_id', '=', 'food_order.id'],
            ])
            ->leftJoin('foods as f', 'f.id', '=', 'foil.item_id')
            ->select([
                'food_order.id as foodOrderId',
                'f.id as foodOrderItemId',
                'f.name as foodName',
                'food_order.table_id as tableId',
                'foil.qty as quantity',
            ])->where(['foi.status' => FoodOrderItem::STATUS_CONFIRMED])->orderBy('order_date')->get();

        return $orders;
    }

    /**
     * Delete records by condition
     *
     * @param array $condition
     * @throws \Exception
     */
    public function deleteByCondition($condition)
    {
        FoodOrderItemLog::where($condition)->delete();
    }

    public function findAllByCondition($where)
    {
        $foodLog = FoodOrderItemLog::where($where)->get();

        return $foodLog;
    }
}