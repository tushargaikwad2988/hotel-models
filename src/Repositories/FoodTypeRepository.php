<?php
namespace HotelModels\HotelModels;

use App\FoodType;

class FoodTypeRepository
{
    /**
     * Get all list of food type
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function findAll()
    {
        $foodTypeList = FoodType::all();

        return $foodTypeList;
    }
}