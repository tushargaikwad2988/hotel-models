<?php
namespace HotelModels\HotelModels;

use App\FoodCategory;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Mockery\CountValidator\Exception;

class FoodCategoryRepository
{
    /**
     * Get all list of food category
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function findAll()
    {
        $foodList = FoodCategory::all();

        return $foodList;
    }

    /**
     * Store new food category
     *
     * @param string $category
     * @return boolean
     */
    public function store($category)
    {
        try {
            $foodCategory = new FoodCategory();
            $foodCategory->name = $category;
            $foodCategory->setCreatedAt(Carbon::now());
            $foodCategory->setUpdatedAt(Carbon::now());

            $foodCategory->save();
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * Find by name
     *
     * @param string $category
     * @return mixed
     */
    public function findByName($category)
    {
        $category = FoodCategory::where(['name' => $category])->get()->first();

        return $category;
    }

    /**
     * Find by id
     * @param integer $categoryId
     */
    public function findById($categoryId)
    {
        $category = FoodCategory::find($categoryId);

        return $category;
    }

    /**
     * Update food category details after initialization
     *
     * @param array $updateField
     * @param integer $id
     */
    public function updateByCondition($updateField, $id)
    {
        try {
            $updateStatus = DB::table('food_category')
                ->where('id', $id)
                ->update($updateField);
        } catch (Exception $e) {
            return false;
        }


        return $updateStatus;
    }
}