<?php
namespace HotelModels\HotelModels;

use HotelModels\HotelModels\FoodOrder;
use HotelModels\HotelModels\FoodOrderItem;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Modules\Hotel\Repositories\HotelTablesRepository;

class FoodOrderRepository
{
    /**
     * Create blank order for new customer
     *
     * @param array $data
     * @return boolean|integer $createOrderStatus
     */
    public function createOrder($data)
    {
        $tableAvail = true;
        if(0 != $data['tableId']) {
            $tableAvail = $this->checkTableAvailable($data['hotelId'], $data['tableId']);
        }

        if (true == $tableAvail) {
            try {
                $foodOrder = new FoodOrder();
                //$orderDate = session()->get('order_date') . ' ' . date('h:i:s');
                $orderDate = Carbon::now();
                $foodOrder->bill_number = 0;
                $foodOrder->status = $data['status'];
                $foodOrder->hotel_id = $data['hotelId'];
                $foodOrder->user_id = $data['userId'];
                $foodOrder->table_id = $data['tableId'];
                $foodOrder->coupon = '';
                $foodOrder->void = $data['voidStatus'];
                $foodOrder->order_date = $orderDate;
                $foodOrder->created_at = Carbon::now();
                $foodOrder->updated_at = Carbon::now();

                $foodOrder->save();
            } catch (Exception $e) {
                return false;
            }

            return $foodOrder->id;
        }

        return false;
    }

    /**
     * Find hotel table
     *
     * @param integer $hotelId
     * @param integer $tableId
     * @return boolean $isAvail
     */
    public function checkTableAvailable($hotelId, $tableId)
    {
        $isAvail = false;
        $orderId = 0;
        $hotelTablesRepository = new HotelTablesRepository();
        $hotelTable = $hotelTablesRepository->findByCondition([
            'hotel_id' => $hotelId,
            'table_id' => $tableId,
        ]);

        if ($hotelTable) {
            $orderId = $hotelTable->order_id;
        }

        if (0 == $orderId) {
            $isAvail = true;
        }

        return $isAvail;
    }

    /**
     * Find all active orders
     *
     * @param integer $userId
     * @return mixed
     */
    public function findActiveOrders($userId)
    {
        $foodOrder = FoodOrder::where('status', '<>', FoodOrder::ORDER_STATUS_CANCELLED)
            ->where('status', '<>', FoodOrder::ORDER_STATUS_PAID)
            ->where('user_id', '=', $userId)->get();

        return $foodOrder;
    }

    /**
     * Find all active orders
     *
     * @param integer $userId
     * @return mixed
     */
    public function findInitializeOrders($userId)
    {
        $foodOrder = FoodOrder::where('status', '<>', FoodOrder::ORDER_STATUS_CANCELLED)
            ->where('status', '=', FoodOrder::ORDER_STATUS_SAVE)
            ->where('table_id', '=', 0)
            ->where('user_id', '=', $userId)->get();

        return $foodOrder;
    }

    /**
     * Update order details after initialization
     *
     * @param array $updateField
     * @param integer $orderId
     */
    public function updateByCondition($updateField, $orderId)
    {
        DB::table('food_order')
            ->where('id', $orderId)
            ->update($updateField);
    }

    /**
     * Find by order id
     *
     * @param integer $orderId
     * @return array
     */
    public function findById($orderId)
    {
        $orderDetails = FoodOrder::find($orderId);

        return $orderDetails;
    }

    /**
     * Find orders by hotel id
     *
     * @param array $conditions
     * @param integer $paginate
     * @param string $fromDate
     * @param string $toDate
     * @return mixed
     */
    public function findOrdersByConditions($conditions, $paginate = 50, $fromDate = null, $toDate = null)
    {
        $orders = FoodOrder::where($conditions)
            ->join('users as u', 'u.id', '=', 'food_order.user_id')
            ->select('food_order.*', 'u.name')
            ->where(function($orders) use($fromDate, $toDate) {
                if (!is_null($fromDate) && !is_null($toDate)) {
                    $orders->whereBetween('order_date', [
                        date('Y-m-d H:i:s', strtotime($fromDate . ' 00:00:00')),
                        date('Y-m-d H:i:s', strtotime($toDate . ' 00:00:00'))
                    ]);
                }
            })
            ->orderBy('order_date', 'desc')->paginate($paginate);

        return $orders;
    }

    /**
     * Get all paid orders by hotel id
     *
     * @param integer $hotelId
     * @return mixed
     */
    public function findPaidOrders($hotelId)
    {
        $orders = FoodOrder::where([
            'status' => FoodOrder::ORDER_STATUS_PAID,
            'hotel_id' =>$hotelId,
        ])->get();

        return $orders;
    }

    /**
     * Get orders from current week
     */
    public function getCurrentWeekOrders()
    {
        $startDate = date("Y-m-d h:i:s", strtotime('monday this week'));
        $endDate = date("Y-m-d h:i:s", strtotime('sunday this week'));

        $orders = FoodOrder::where(['status' => FoodOrder::ORDER_STATUS_PAID])->whereBetween('created_at', [$startDate, $endDate])->get();

        return $orders;
    }

    /**
     * Find food order item by condition
     *
     * @param integer $tableId
     * @param integer $hotelId
     * @return array $foodOrderDetails
     */
    public function findByTable($tableId, $hotelId)
    {
        $foodOrderDetails = FoodOrder::where([
            'table_id' => $tableId,
            'hotel_id' => $hotelId,
            'user_id' => Auth::id(),
        ])
            ->where('status', '<>', FoodOrder::ORDER_STATUS_PAID)->first();

        return $foodOrderDetails;
    }

    /**
     * Get orders for catering department
     *
     * @param integer $hotelId
     * @param boolean $isPantry
     * @return \Illuminate\Support\Collection
     */
    public function getCateringOrders($hotelId, $isPantry = false)
    {
        $orders = FoodOrder::where([
            'food_order.hotel_id' => $hotelId,
            'foil.status' => FoodOrder::ORDER_STATUS_CONFIRMED,
        ])
            ->leftJoin('food_order_item_log as foil', [
                ['foil.food_order_id', '=', 'food_order.id'],
            ])
            ->leftJoin('foods as f', 'f.id', '=', 'foil.item_id')
            ->select([
                'food_order.id as foodOrderId',
                'f.id as foodOrderItemId',
                'f.name as foodName',
                'food_order.table_id as tableId',
                'foil.qty as quantity',
            ])->where([
                'foil.status' => FoodOrderItem::STATUS_CONFIRMED,
                'f.is_pantry' => $isPantry
            ])->orderBy('order_date')->get();

        return $orders;
    }

    public function findByCondition($conditions)
    {
        return FoodOrder::where($conditions)->first();
    }

    /**
     * Get parcel order
     *
     * @param integer $hotelId
     * @return \Illuminate\Support\Collection
     */
    public function getParcelOrders($hotelId)
    {
        return FoodOrder::where([
            'hotel_id' => $hotelId,
            'table_id' => 0,
        ])->where('order_date', '>', date('Y-m-d 00:00:00'))->where('status', '<>', config('constants.order_status.paid'))->get();
    }

    /**
     * Find last order by table id
     *
     * @param integer $tableId
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function findLastOrderByTable($tableId)
    {
        return FoodOrder::where(['table_id'=> (int)$tableId])->latest()->first();
    }
}