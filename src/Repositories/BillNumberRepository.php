<?php
namespace HotelModels\HotelModels;

use HotelModels\HotelModels\BillNumber;
use Illuminate\Support\Facades\DB;

class BillNumberRepository
{
    public function initializeBillNumber($hotelId = 0)
    {
        if (0 == (int)$hotelId) {
            return 0;
        }

        $this->updateByCondition(['bill_number' => 0], $hotelId);

        return true;
    }

    /**
     * Find data by condition
     *
     * @param array $condition
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function findByCondition($condition)
    {
        return BillNumber::where($condition)->first();
    }

    /**
     * Update order details after initialization
     *
     * @param array $updateField
     * @param integer $hotelId
     */
    public function updateByCondition($updateField, $hotelId)
    {
        DB::table('bill_number')
            ->where('hotel_id', $hotelId)
            ->update($updateField);
    }

    public function createBillNumber($hotelId = 0)
    {
        if (0 >= (int)$hotelId) {
            return false;
        }

        BillNumber::where('hotel_id', $hotelId)->increment('bill_number');
    }
}