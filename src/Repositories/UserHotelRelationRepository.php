<?php
namespace HotelModels\HotelModels;

use App\UserHotelRelation;
use App\Hotel;

class UserHotelRelationRepository
{
    /**
     * Check in relation exists
     *
     * @param integer $hotelId
     * @param integer $userId
     * @return boolean
     */
    public function relationExists($hotelId, $userId)
    {
        if (in_array(0, [$hotelId, $userId])) {
            return false;
        }

        $userHotelRelation = new UserHotelRelation();
        $relation = $userHotelRelation::where([
            'hotel_id' => $hotelId,
            'user_id' => $userId,
        ])->get();

        if (count($relation->toArray()) > 0) {
            return true;
        }

        return false;
    }

    /**
     * Create relation between hotel and user
     *
     * @param integer $hotelId
     * @param integer $userId
     * @return UserHotelRelation|bool
     */
    public function createRelation($hotelId, $userId)
    {
        if (in_array(0, [$hotelId, $userId])) {
            return false;
        }

        $userHotelRelation = new UserHotelRelation();
        $userHotelRelation->hotel_id = $hotelId;
        $userHotelRelation->user_id = $userId;
        $userHotelRelation->created_at = date('Y-m-d h:i:s');
        $userHotelRelation->updated_at = date('Y-m-d h:i:s');

        try {
            $userHotelRelation->save();
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return $userHotelRelation;
    }

    /**
     * Get hotel id by user id
     *
     * @param integer $userId
     * @return array|boolean
     */
    public function getHotelIdByUserId($userId)
    {
        if (0 == (int)$userId) {
            return false;
        }

        $userHotelRelation = new UserHotelRelation();
        $result = $userHotelRelation::where(['user_id' => $userId])->first();

        if ($result) {
            return $result;
        }

        return [];
    }

    /**
     * Get all hotel list by user id
     *
     * @param integer $userId
     * @param bool $onlyActive
     * @return array
     */
    public function getAllHotelsByUserId($userId, $onlyActive = false)
    {
        if (0  == (int)$userId) {
            return false;
        }

        $userHotelRelation = new UserHotelRelation();
        $relations = $userHotelRelation::where([
            'user_id' => $userId,
        ])->join('hotel as h', 'h.id', '=', 'user_hotel_relation.hotel_id');

        if ($onlyActive) {
            $relations->where('h.status', '=', Hotel::STATUS_ACTIVE);
        }

        $result = $relations->get();

        return $result;
    }
}