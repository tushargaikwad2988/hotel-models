<?php
namespace HotelModels\HotelModels;

use HotelModels\HotelModels\DeliveryDetails;
use Carbon\Carbon;
use Mockery\CountValidator\Exception;

class DeliveryDetailsRepository
{
    /**
     * Save delivery details
     *
     * @param array $data
     * @return bool|mixed
     */
    public function save($data)
    {
        try {
            $deliveryDetails = new DeliveryDetails();
            $deliveryDetails->setFoodOrderId($data['orderId']);
            $deliveryDetails->setNumber($data['delivery']['number']);
            $deliveryDetails->setAddress($data['delivery']['address']);
            $deliveryDetails->setCreatedAt(Carbon::now());
            $deliveryDetails->setUpdatedAt(Carbon::now());

            $deliveryDetails->save();
        } catch (Exception $e) {
            return false;
        }

        return $deliveryDetails->getQueueableId();
    }

    /**
     * Find by condition
     *
     * @param array $condition
     * @return mixed
     */
    public function findByCondition($condition)
    {
        $deliveryDetails = DeliveryDetails::where($condition)->get()->first();

        return $deliveryDetails;
    }

    /**
     * Delete delivery by id
     *
     * @param integer $deliveryId
     */
    public function deleteDelivery($deliveryId)
    {
        DeliveryDetails::destroy($deliveryId);
    }
}