<?php
namespace HotelModels\HotelModels;

use HotelModels\HotelModels\CustomerAddress;
use Illuminate\Support\Facades\DB;
use Mockery\CountValidator\Exception;

class CustomerAddressRepository
{
    public function createAddress($data)
    {
        $customerAddress = new CustomerAddress();
        $customerAddress->setAddress($data['address']);
        $customerAddress->setName($data['name']);
        $customerAddress->setAlternateMobile($data['altMobile']);
        $customerAddress->setLandmark($data['landmark']);
        $customerAddress->setAddressType($data['addressType']);
        $customerAddress->setMobile($data['mobile']);
        $customerAddress->setUserId($data['userId']);

        try {
            $customerAddress->save();
        } catch (Exception $e) {
            return false;
        }

        return true;
    }
}