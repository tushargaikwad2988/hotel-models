<?php
namespace HotelModels\HotelModels;

use App\Hotel;
use App\Organization;
use Mockery\CountValidator\Exception;

class HotelRepository
{
    /**
     * Save hotel resource
     *
     * @param Request $data
     * @return integer $id
     */
    public function save($data)
    {
        $hotel = new Hotel();
        $hotel->name = $data->name;
        $hotel->address_line_1 = $data->addressLine1;
        $hotel->address_line_2 = $data->addressLine2;
        $hotel->address_line_3 = $data->addressLine3;
        $hotel->std_code = config('custom.std_code');
        $hotel->telephone = $data->telephone;
        $hotel->mobile = $data->mobile;
        $hotel->rooms = 0;
        $hotel->services = $data->services;
        $hotel->gst = $data->gst;
        $hotel->status = Hotel::STATUS_ACTIVE;

        $hotel->save();

        return $hotel->id;
    }

    /**
     * Find all resource by condition
     *
     * @param integer $authId
     * @return array
     */
    public function getActiveUserHotelList($authId)
    {
        $hotel = Hotel::select('hotel.*')
            //->join('organization as o', 'o.id', '=', 'hotel.organization_id')
            //->where('o.user_id', '=', $authId)
            //->where('o.status', '=', Organization::STATUS_ACTIVE)
            ->get();

        return $hotel;
    }

    /**
     * Update hotel resource
     *
     * @param Request $data
     * @param integer $hotelId
     * @return boolean
     */
    public function update($data, $hotelId)
    {
        if (empty($hotelId)) {
            return false;
        }

        $hotel = Hotel::find($hotelId);
        $hotel->name = $data->name;
        $hotel->address_line_1 = $data->addressLine1;
        $hotel->address_line_2 = $data->addressLine2;
        $hotel->address_line_3 = $data->addressLine3;
        $hotel->telephone = $data->telephone;
        $hotel->mobile = $data->mobile;
        $hotel->gst = $data->gst;

        return $hotel->update();
    }

    /**
     * Update table field on condition
     *
     * @param string $fieldName
     * @param string $fieldValue
     * @param integer $hotelId
     * @return mixed
     */
    public function updateField($fieldName, $fieldValue, $hotelId)
    {
        $hotel = new Hotel();
        try{
            return $hotel::find($hotelId)->update([$fieldName => $fieldValue]);
        } catch (Exception $e) {
            echo $e->getMessage();
        }

    }

    /**
     * Find hotel by hotel id
     *
     * @param integer $hotelId
     * @return array
     */
    public function findByHotelId($hotelId)
    {
        if (0 == (int)$hotelId) {
            return [];
        }

        $hotel = new Hotel();
        $hotelDetails = $hotel::where([
            'id' => $hotelId,
            'status' => Hotel::STATUS_ACTIVE
        ])->first();

        if ($hotelDetails) {
            return $hotelDetails;
        }

        return [];
    }

    /**
     * Find hotel by hotel id
     *
     * @param integer $hotelId
     * @return array
     */
    public function findById($hotelId)
    {
        if (0 == (int)$hotelId) {
            return [];
        }

        $hotel = new Hotel();
        $hotelDetails = $hotel::where([
            'id' => $hotelId,
        ])->get()->first();

        if ($hotelDetails) {
            return $hotelDetails;
        }

        return [];
    }

    /**
     * Find by condition
     *
     * @param array $condition
     * @return mixed
     */
    public function findByCondition($condition)
    {
        $hotelList = Hotel::where($condition)->get();

        return $hotelList;
    }

    /**
     * Find all active hotel
     *
     * @return mixed
     */
    public function findAllActiveHotel()
    {
        $hotel = Hotel::select('hotel.*')
            ->where('hotel.status', '=', Hotel::STATUS_ACTIVE)
            ->get();

        return $hotel;
    }
}