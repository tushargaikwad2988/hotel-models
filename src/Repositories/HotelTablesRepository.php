<?php
namespace HotelModels\HotelModels;

use App\HotelTables;

class HotelTablesRepository
{
    /**
     * Create table for hotels
     *
     * @param integer $noOfTables
     * @param integer $hotelId
     * @return boolean
     */
    public function createTables($noOfTables, $hotelId)
    {
        if (0 == (int)$noOfTables) {
            return false;
        }

        for ($index = 1; $index <= $noOfTables; $index++) {
            $hotelTables = new HotelTables();
            $hotelTables->hotel_id = $hotelId;
            $hotelTables->table_id = $index;
            $hotelTables->save();
        }

        return true;
    }

    /**
     * Find hotel table by condition
     *
     * @param array $condition
     * @return mixed
     */
    public function findByCondition($condition)
    {
        $hotelTable = HotelTables::where($condition)->first();

        return $hotelTable;
    }

    /**
     * Delete tables from hotel
     *
     * @param integer $hotelId
     */
    public function deleteTables($hotelId)
    {
        HotelTables::where(['hotel_id' => $hotelId])->delete();
    }

    /**
     * Update hotel table
     *
     * @param integer $hotelId
     * @param integer $tableId
     * @param integer $orderId
     * @return boolean
     */
    public function updateHotelTable($hotelId, $tableId, $orderId)
    {
        HotelTables::where([
            'hotel_id' => $hotelId,
            'table_id' => $tableId,
        ])->update(['order_id' => $orderId]);

        return true;
    }

    /**
     * Find hotel tables by condition
     *
     * @param array $condition
     * @return mixed
     */
    public function findAll($condition)
    {
        $hotelTable = HotelTables::where($condition)->get();

        return $hotelTable;
    }
}