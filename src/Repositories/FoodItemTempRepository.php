<?php
namespace HotelModels\HotelModels;

use App\FoodItemTemp;

class FoodItemTempRepository
{
    /**
     * Get all list of temporary food item list
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function findAll()
    {
        $foodList = FoodItemTemp::all();

        return $foodList;
    }

    /**
     * Store all records from food item temp list
     *
     * @param array $data
     * @param integer $hotelId
     * @return integer $result
     */
    public function saveAll($data, $hotelId)
    {
        $itemObj = new \ArrayObject($data);
        $iterator = $itemObj->getIterator();
        while ($iterator->valid()) {
            $item = $iterator->current();
            if (!empty($item['category'])) {
                $foodItemTemp = FoodItemTemp::firstOrNew([
                    'hotel_id' => $hotelId,
                    'name' => $item['name']
                ]);
                $foodItemTemp->hotel_id = (int)$hotelId;
                $foodItemTemp->category_id = $item['category'];
                $foodItemTemp->name = $item['name'];
                $foodItemTemp->abbreviation = $item['abbreviation'];
                $foodItemTemp->description = $item['description'];
                $foodItemTemp->amount = $item['price'];
                $foodItemTemp->discount = 0;
                $foodItemTemp->is_pantry = $item['pantry'];
                $result = $foodItemTemp->save();
                $iterator->next();
            }
        }

        return $result;
    }

    /**
     * Find all records on condition
     *
     * @param array $condition
     * @throws InvalidArgumentException
     * @return Illuminate\Pagination\LengthAwarePaginator
     */
    public function findAllByCondition($condition)
    {
        if ([] === $condition) {
            throw new InvalidArgumentException();
        }

        return FoodItemTemp::with('foodCategory')->where($condition)->get();
    }

    /**
     * Delete records by hotel id
     *
     * @param integer $hotelId
     * @return boolean $deletedRows
     */
    public function deleteByHotelId($hotelId)
    {
        if (0 === (int)$hotelId) {
            return false;
        }

        $deletedRows = FoodItemTemp::where('hotel_id', $hotelId)->delete();

        return $deletedRows;
    }
}