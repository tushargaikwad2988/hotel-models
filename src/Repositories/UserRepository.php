<?php
namespace HotelModels\HotelModels;

use App\User;
use Illuminate\Support\Facades\Auth;
use Mockery\CountValidator\Exception;

class UserRepository
{
    /**
     * Create user
     *
     * @param $data
     * @return bool|mixed
     */
    public function createUser($data)
    {
        if ([] === $data) {
            return false;
        }

        $existingUser = $this->checkUserExists($data);

        if (empty($existingUser)) {
            return $existingUser->id;
        }

        $user = new User();
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = bcrypt($data['password']);
        $user->user_type_id = $data['typeId'];
        $user->parent_id = Auth::Id();

        try {
            $user->save();
        } catch (Exception $e) {
            return false;
        }

        return $user->id;
    }

    /**
     * Check if user already exists with there email id and name
     *
     * @param array $data
     * @return array $user
     */
    public function checkUserExists($data)
    {
        $user = User::where(['name' => $data['name'], 'email' => $data['email']])->get();

        return $user;
    }

    /**
     * Update user details
     *
     * @param array $data
     * @return boolean
     */
    public function update($data)
    {
        if ([] == $data) {
            return false;
        }

        $userDetails = User::find($data['userId']);

        //$password = $this->checkPassword($userDetails->password, $data['password']);

        if (empty($userDetails)) {
            return false;
        }
        $newPassword = bcrypt($data['password']);

        try {
            $userDetails->name = $data['name'];
            $userDetails->email = $data['email'];
            $userDetails->user_type_id = $data['typeId'];
            if (strlen($data['password']) == config('custom.password') && $userDetails->password != $newPassword) {
                $userDetails->password = $newPassword;
            }

            $userDetails->save();
        } catch (Exception $e) {
            return false;
        }


        return $userDetails;
    }

    public function checkPassword($oldPassword, $newPassword)
    {
        if (strlen($newPassword) == config('custom.password') && $oldPassword != bcrypt($newPassword)) {
            return $newPassword;
        }

        return bcrypt($oldPassword);
    }

    public function getCustomerDetails($userId)
    {
        $user = User::where(['id' => $userId])->with('customerAddress')->first();

        return $user;
    }
}