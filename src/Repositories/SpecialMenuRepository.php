<?php
namespace HotelModels\HotelModels;

use App\SpecialMenu;

class SpecialMenuRepository
{
    /**
     * Find records by condition
     *
     * @param array $condition
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public function findByCondition($condition)
    {
        $list = SpecialMenu::where($condition)->with(['food'])->get();

        return $list;
    }

    /**
     * Find one by condition
     *
     * @param array $condition
     * @return array $list
     */
    public function findOneByCondition($condition)
    {
        $list = SpecialMenu::where($condition)->with(['food'])->first();

        return $list;
    }

    /**
     * Save special menu item
     *
     * @param array $food
     * @param integer $hotelId
     * @return boolean
     */
    public function save($food, $hotelId)
    {
        try {
            $specialMenu = new SpecialMenu();
            $specialMenu->setFoodId($food->id);
            $specialMenu->setHotelId($hotelId);
            $specialMenu->setAmount($food->getAmount());
            $specialMenu->setDescription($food->getDescription());
            $specialMenu->setDiscount($food->getDiscount());

            $specialMenu->save();
        } catch (Exception $e) {
            return false;
        }

        return $specialMenu;
    }
}