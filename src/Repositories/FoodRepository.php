<?php
namespace HotelModels\HotelModels;

use HotelModels\HotelModels\Food;

class FoodRepository
{
    /**
     * Save food
     *
     * @param array $data
     * @return boolean
     */
    public function save($data)
    {
        $food = Food::firstOrNew([
            'hotel_id' => $data['hotel_id'],
            'name' => $data['name']
        ]);
        $food->hotel_id = $data['hotel_id'];
        $food->category_id = $data['category_id'];
        $food->name = $data['name'];
        $food->abbreviation = $data['abbreviation'];
        $food->description = $data['description'];
        $food->amount = $data['amount'];
        $food->discount = $data['discount'];
        $food->is_pantry = $data['is_pantry'];
        $result = $food->save();

        return $result;
    }

    /**
     * Find all records on condition
     *
     * @param array $condition
     * @throws InvalidArgumentException
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function findAllByCondition($condition)
    {
        if ([] === $condition) {
            throw new InvalidArgumentException();
        }

        return Food::select('foods.*', 'fc.name as category')
            ->join('food_category as fc', 'fc.id', '=', 'foods.category_id')
            ->where($condition)
            ->get();
    }

    /**
     * Search food item from list
     *
     * @param integer $hotelId
     * @param string $searchKey
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function searchFoodItem($hotelId, $searchKey)
    {
        $foodList = Food::select([
            'id' => 'foods.id',
            'name' => 'foods.name',
            'abbreviation' => 'foods.abbreviation',
            'amount' => 'foods.amount',
            'discount' => 'foods.discount',
        ])->where(['hotel_id' => $hotelId, 'status' => Food::STATUS_ACTIVE])
        ->where(function($q) use($searchKey){
            $q->where('foods.id', '=', $searchKey)
                ->orWhere('abbreviation', 'like', $searchKey . '%')
                ->orWhere('name', 'like', $searchKey . '%');
        });

        $result = $foodList->get();

        return $result;
    }

    /**
     * Find all records on condition
     *
     * @param array $condition
     * @throws InvalidArgumentException
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function findAllIdsByCondition($condition)
    {
        if ([] === $condition) {
            throw new InvalidArgumentException();
        }

        return Food::select('foods.id')
            ->join('food_category as fc', 'fc.id', '=', 'foods.category_id')
            ->where($condition)
            ->get();
    }

    /**
     * Update table field on condition
     *
     * @param string $fieldName
     * @param string $fieldValue
     * @param integer $foodId
     * @return mixed
     */
    public function updateField($fieldName, $fieldValue, $foodId)
    {
        $food = new Food();

        return $food::find($foodId)->update([$fieldName => $fieldValue]);
    }

    /**
     * Find food item by id
     *
     * @param integer $foodId
     * @return mixed
     */
    public function findById($foodId)
    {
        return Food::where(['id' => $foodId])->first();
    }

    /**
     * Find all food by condition
     *
     * @param array $conditions
     * @param string $orderBy
     * @param string $orderByField
     * @return mixed
     */
    public function findByCondition($conditions, $orderBy = 'asc', $orderByField = 'name')
    {
        $foods = Food::where($conditions)->orderBy($orderByField, $orderBy)->get();

        return $foods;
    }
}