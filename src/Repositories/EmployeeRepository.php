<?php
namespace HotelModels\HotelModels;

use App\User;
use App\Employee;
use App\UserHotelRelation;
use Mockery\CountValidator\Exception;

class EmployeeRepository
{
    /**
     * Create employee resource
     *
     * @param array $data
     * @param integer $userId
     * @return boolean
     */
    public function createEmployee(array $data, $userId)
    {
        if ([] === $data) {
            return false;
        }

        $employee = new Employee();
        $employee->user_id = $userId;
        $employee->birth_date = date('Y-m-d', strtotime($data['birthDate']));
        $employee->mobile = $data['mobile'];
        $employee->alt_mobile = $data['altMobile'];
        $employee->address = $data['address'];
        $employee->address_proof_id = isset($data['addressProofId']) ? $data['addressProofId'] : 1;
        $employee->identity_proof_id = isset($data['identityProofId']) ? $data['identityProofId'] : 1;
        $employee->profile_image = '';
        $employee->document_image = '';
        $employee->status = Employee::STATUS_ACTIVE;

        try {
            $employee->save();
        } catch (Exception $e) {
            return false;
        }

        return $employee;
    }

    /**
     * Find all by hotel id
     *
     * @param integer $hotelId
     * @return Illuminate\Database\Eloquent\Collection $employeeList
     */
    public function findAllByHotel($hotelId)
    {
        $employeeList = UserHotelRelation::select(
            'e.*',
            'u.name as name',
            'u.email as email',
            'ut.name as userType',
            'h.name as hotelName'
        )
            ->leftJoin('users as u', 'u.id', '=', 'user_hotel_relation.user_id')
            ->leftJoin('employee as e', 'e.user_id', '=', 'user_hotel_relation.user_id')
            ->leftJoin('user_type as ut', 'u.user_type_id', '=', 'ut.id')
            ->leftJoin('hotel as h', 'h.id', '=', 'user_hotel_relation.hotel_id')
            ->where('user_hotel_relation.hotel_id', '=', $hotelId)
            ->where('u.user_type_id', '<>',  '1')
            ->get();

        return $employeeList;
    }

    /**
     * Find all employee which are parent id of same login
     *
     * @param $parentId
     * @param boolean $onlyActive
     */
    public function findAllEmployeeWithoutResource($parentId, $onlyActive = false)
    {
        $employee = User::select(
            'e.*',
            'users.*',
            'ut.name as userType',
            'h.name as hotelName',
            'e.id as empId'
        )
            ->leftJoin('employee as e', 'e.user_id', '=', 'users.id')
            ->leftJoin('user_type as ut', 'users.user_type_id', '=', 'ut.id')
            ->leftJoin('user_hotel_relation as utr', 'utr.user_id', '=', 'users.id')
            ->leftJoin('hotel as h', 'h.id', '=', 'utr.hotel_id')
            ->where('users.id', '<>',  '1')
            ->where('users.parent_id', '=', $parentId);
        if ($onlyActive) {
            $employee->where('e.status', '=', Employee::STATUS_ACTIVE);
        }
        $result = $employee->get();

        return $result;
    }

    /**
     * Find employee by id
     *
     * @param array $condition
     * @return array
     */
    public function findByCondition($condition)
    {
        if ([] == $condition) {
            return [];
        }

        $employee = new Employee();
        $employeeDetails = $employee::find($condition);

        if ($employeeDetails) {
            return $employeeDetails;
        }

        return [];
    }

    /**
     * Update table field on condition
     *
     * @param string $fieldName
     * @param string $fieldValue
     * @param integer $empId
     * @return mixed
     */
    public function updateField($fieldName, $fieldValue, $empId)
    {
        $employee = new Employee();

        return $employee::find($empId)->update([$fieldName => $fieldValue]);
    }

    /**
     * Update employee details
     *
     * @param array $data
     * @return boolean
     */
    public function update($data)
    {
        if ([] == $data) {
            return false;
        }

        $employee = Employee::find($data['empId']);

        if (empty($employee)) {
            return false;
        }

        try {
            $employee->birth_date = date('Y-m-d', strtotime($data['birthDate']));
            $employee->address_proof_id = isset($data['addressProofId']) ? $data['addressProofId'] : 1;
            $employee->identity_proof_id = isset($data['identityProofId']) ? $data['identityProofId'] : 1;
            $employee->mobile = $data['mobile'];
            $employee->alt_mobile = $data['altMobile'];
            $employee->address = $data['address'];

            $employee->save();
        } catch (Exception $e) {
            return false;
        }

        return true;
    }
}