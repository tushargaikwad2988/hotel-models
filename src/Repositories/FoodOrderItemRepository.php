<?php
namespace HotelModels\HotelModels;

use HotelModels\HotelModels\FoodOrderItem;
use Carbon\Carbon;
use Mockery\CountValidator\Exception;

class FoodOrderItemRepository
{
    /**
     * Save food item for respective order
     *
     * @param array $data
     * @param integer $orderId
     * @param integer $status
     * @return bool|mixed
     */
    public function save($data, $orderId, $status = FoodOrderItem::STATUS_CONFIRMED)
    {
        try {
            $foodOrderItem = new FoodOrderItem();
            $foodOrderItem->food_order_id = $orderId;
            $foodOrderItem->item_id = $data['id'];
            $foodOrderItem->qty = $data['qty'];
            $foodOrderItem->unit_price = $data['unitPrice'];
            $foodOrderItem->discount = 0;
            //$foodOrderItem->status = $status;
            $foodOrderItem->created_at = Carbon::now();
            $foodOrderItem->updated_at = Carbon::now();

            $foodOrderItem->save();
        } catch (Exception $e) {
            return false;
        }

        return $foodOrderItem->id;
    }

    /**
     * Find food order item by condition
     *
     * @param array $condition
     * @return FoodOrderItem
     */
    public function findByCondition($condition)
    {
        $foodItemDetails = FoodOrderItem::select('food_order_item.*', 'foods.name', 'foods.is_pantry as pantry')
            ->join('foods', 'foods.id', '=', 'food_order_item.item_id')
            ->where($condition)->get();

        return $foodItemDetails;
    }

    /**
     * Remove product from list by order id
     *
     * @param string $column
     * @param integer $value
     */
    public function destroy($column, $value)
    {
        FoodOrderItem::where($column, $value)->delete();
    }

    /**
     * Update field by condition
     *
     * @param array $condition
     * @param array $field
     * @return boolean $status
     */
    public function updateByCondition($condition, $field)
    {
        $status = FoodOrderItem::where($condition)->update($field);

        return $status;
    }

    /**
     * Find food order item by condition
     *
     * @param array $condition
     * @param integer $paginate
     * @param string $fromDate
     * @param string $toDate
     * @return FoodOrderItem
     */
    public function findAllByCondition($condition, $paginate = 50, $fromDate = null, $toDate = null)
    {
        $foodItemDetails = FoodOrderItem::select('food_order_item.*', 'foods.name', 'fo.order_date', 'fo.bill_number', 'fo.receipt_taken')
            ->join('foods', 'foods.id', '=', 'food_order_item.item_id')
            ->join('food_order as fo', 'fo.id', '=', 'food_order_item.food_order_id')
            ->where($condition)
            ->where(function($foodItemDetails) use($fromDate, $toDate){
                if (!is_null($fromDate) && !is_null($toDate)) {
                    $foodItemDetails->whereBetween('fo.order_date', [
                        date('Y-m-d H:i:s', strtotime($fromDate . ' 00:00:00')),
                        date('Y-m-d H:i:s', strtotime($toDate . ' 12:29:59'))
                    ]);
                }
            })
            ->orderBy('fo.order_date', 'desc')->paginate($paginate);

        return $foodItemDetails;
    }

    /**
     * Find all item by condition with in
     *
     * @param string $field
     * @param array $value
     * @return \Illuminate\Support\Collection
     */
    public function findAllByInCondition($field, $value)
    {
        return FoodOrderItem::whereIn($field, $value)->get();
    }
}